import { event } from './event'

export interface Package {
    name: string,
    id: number,
    price: number,
    destination: string,
    source: string,
    orderedDate: string,
    driverName: string,
    vehicleType: number,
    atWarehouse: event,
    sentFromStore: event,
    arrivedDestination: event,
    isArrived: boolean,
    isSent: boolean,
    remainingTime:number,
    sourceLatLng: {
        lat: number,
        lng: number,
    },
    destinationLatLng: {
        lat: number,
        lng: number,
    },
}