import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(
    private toastr: ToastrService
  ) {
  }

  getCurrentLanguage() {
    return localStorage.getItem('currentLanguage')
  }

  showSuccessToastr(message, title) {
    this.toastr.success(title, message, {
      timeOut: 3000,
      progressBar: true
    });
  }

  showFaildToastr(message, title) {
    this.toastr.error(message, title, {
      timeOut: 3000,
      progressBar: true
    });
  }

}

