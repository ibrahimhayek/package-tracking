import { Injectable } from '@angular/core';
import { Package } from '../../common-models/package';
import { IPackagesBase } from '../reprositories/IPackages';

@Injectable()
export class PackagesMock implements IPackagesBase {

    retrievePackages(): Promise<Package[]> {
        let _Package: Package[];
        _Package = this.dummyPackages;
        return Promise.resolve<Package[]>(_Package);
    }

    dummyPackages = [
        {
            name: "Laptop",
            id: 123,
            price: 200,
            destination: "Regeringsgatan 29,  111 53 Stockholm,  Sweden",
            source: "Karins allé 8,  181 45 Lidingö,  Sweden ",
            sourceLatLng: {
                lat: 59.33923018339606,
                lng: 18.04826259613037,
            },
            destinationLatLng: {
                lat: 59.3320844,
                lng: 18.067956,
            },
            orderedDate: "2021-10-20",
            arrivalDate: null,
            driverName: "Jack",
            vehicleType: 1,
            atWarehouse: {
                startDate: "2021-10-20",
                endDate: "2021-10-21"
            },
            sentFromStore: {
                startDate: "2021-10-21",
                endDate: null
            },
            arrivedDestination: {
                startDate: null,
                endDate: null
            },
            isArrived: false,
            isSent: true,
            remainingTime: 2
        },
        {
            name: "Pencil",
            id: 128,
            price: 200,
            destination: "Birger Jarlsgatan 112,  114 20 Stockholm,  Sweden",
            source: " Norrbackagatan 15,  113 34 Stockholm,  Sweden",
            sourceLatLng: {
                lat: 59.34161634194161,
                lng: 18.030620643702203,
            },
            destinationLatLng: {
                lat: 59.34817291129667,
                lng: 18.060304824719754,
            },
            orderedDate: "2021-10-20",
            arrivalDate: null,
            driverName: "Jack",
            vehicleType: 1,
            atWarehouse: {
                startDate: "2021-10-20",
                endDate: "2021-10-21"
            },
            sentFromStore: {
                startDate: "2021-10-21",
                endDate: null
            },
            arrivedDestination: {
                startDate: null,
                endDate: null
            },
            isArrived: false,
            isSent: true,
            remainingTime: 0.09
        },
        {
            name: "Chair",
            id: 125,
            price: 200,
            destination: "Regeringsgatan 29,  111 53 Stockholm,  Sweden",
            source: " Hornsgatan 43,  118 49 Stockholm,  Sweden ",
            sourceLatLng: {
                lat: 59.31840413169649,
                lng: 18.058631416407277,
            },
            destinationLatLng: {
                lat: 59.3320844,
                lng: 18.067956,
            },
            orderedDate: "2021-10-20",
            arrivalDate: null,
            driverName: "Jack",
            vehicleType: 1,
            atWarehouse: {
                startDate: "2021-10-20",
                endDate: "2021-10-21"
            },
            sentFromStore: {
                startDate: "2021-10-21",
                endDate: null
            },
            arrivedDestination: {
                startDate: null,
                endDate: null
            },
            isArrived: false,
            isSent: false,
            remainingTime: 0.8
        },
        {
            name: "Phone",
            id: 124,
            price: 200,
            destination: "Birger Jarlsgatan 112,  114 20 Stockholm,  Sweden",
            source: " Norrbackagatan 15,  113 34 Stockholm,  Sweden",
            sourceLatLng: {
                lat: 59.34161634194161,
                lng: 18.030620643702203,
            },
            destinationLatLng: {
                lat: 59.34817291129667,
                lng: 18.060304824719754,
            },
            orderedDate: "2021-10-20",
            arrivalDate: "2021-10-21",
            driverName: "Jack",
            vehicleType: 1,
            atWarehouse: {
                startDate: "2021-10-20",
                endDate: "2021-10-21"
            },
            sentFromStore: {
                startDate: "2021-10-21",
                endDate: null
            },
            arrivedDestination: {
                startDate: null,
                endDate: null
            },
            isArrived: true,
            isSent: true,
            remainingTime: 0
        },
        {
            name: "Book",
            id: 126,
            price: 200,
            destination: "Birger Jarlsgatan 112,  114 20 Stockholm,  Sweden",
            source: " Norrbackagatan 15,  113 34 Stockholm,  Sweden",
            sourceLatLng: {
                lat: 59.34161634194161,
                lng: 18.030620643702203,
            },
            destinationLatLng: {
                lat: 59.34817291129667,
                lng: 18.060304824719754,
            },
            orderedDate: "2021-10-20",
            arrivalDate: "2021-10-21",
            driverName: "Jack",
            vehicleType: 1,
            atWarehouse: {
                startDate: "2021-10-20",
                endDate: "2021-10-21"
            },
            sentFromStore: {
                startDate: "2021-10-21",
                endDate: null
            },
            arrivedDestination: {
                startDate: null,
                endDate: null
            },
            isArrived: true,
            isSent: true,
            remainingTime: 0
        },
        {
            name: "Fan",
            id: 127,
            price: 200,
            destination: "Birger Jarlsgatan 112,  114 20 Stockholm,  Sweden",
            source: " Norrbackagatan 15,  113 34 Stockholm,  Sweden",
            sourceLatLng: {
                lat: 59.34161634194161,
                lng: 18.030620643702203,
            },
            destinationLatLng: {
                lat: 59.34817291129667,
                lng: 18.060304824719754,
            },
            orderedDate: "2021-10-20",
            arrivalDate: "2021-10-21",
            driverName: "Jack",
            vehicleType: 1,
            atWarehouse: {
                startDate: "2021-10-20",
                endDate: "2021-10-21"
            },
            sentFromStore: {
                startDate: "2021-10-21",
                endDate: null
            },
            arrivedDestination: {
                startDate: null,
                endDate: null
            },
            isArrived: true,
            isSent: true,
            remainingTime: 0
        },
        {
            name: "Toy",
            id: 129,
            price: 200,
            destination: "Birger Jarlsgatan 112,  114 20 Stockholm,  Sweden",
            source: " Norrbackagatan 15,  113 34 Stockholm,  Sweden",
            sourceLatLng: {
                lat: 59.34161634194161,
                lng: 18.030620643702203,
            },
            destinationLatLng: {
                lat: 59.34817291129667,
                lng: 18.060304824719754,
            },
            orderedDate: "2021-10-20",
            arrivalDate: "2021-10-21",
            driverName: "Jack",
            vehicleType: 1,
            atWarehouse: {
                startDate: "2021-10-20",
                endDate: "2021-10-21"
            },
            sentFromStore: {
                startDate: "2021-10-21",
                endDate: null
            },
            arrivedDestination: {
                startDate: null,
                endDate: null
            },
            isArrived: true,
            isSent: true,
            remainingTime: 0
        },
        {
            name: "PS4",
            id: 130,
            price: 200,
            destination: "Birger Jarlsgatan 112,  114 20 Stockholm,  Sweden",
            source: " Norrbackagatan 15,  113 34 Stockholm,  Sweden",
            sourceLatLng: {
                lat: 59.34161634194161,
                lng: 18.030620643702203,
            },
            destinationLatLng: {
                lat: 59.34817291129667,
                lng: 18.060304824719754,
            },
            orderedDate: "2021-10-20",
            arrivalDate: "2021-10-21",
            driverName: "Jack",
            vehicleType: 1,
            atWarehouse: {
                startDate: "2021-10-20",
                endDate: "2021-10-21"
            },
            sentFromStore: {
                startDate: "2021-10-21",
                endDate: null
            },
            arrivedDestination: {
                startDate: null,
                endDate: null
            },
            isArrived: true,
            isSent: true,
            remainingTime: 0
        },
        {
            name: "Milk",
            id: 131,
            price: 200,
            destination: "Birger Jarlsgatan 112,  114 20 Stockholm,  Sweden",
            source: " Norrbackagatan 15,  113 34 Stockholm,  Sweden",
            sourceLatLng: {
                lat: 59.34161634194161,
                lng: 18.030620643702203,
            },
            destinationLatLng: {
                lat: 59.34817291129667,
                lng: 18.060304824719754,
            },
            orderedDate: "2021-10-20",
            arrivalDate: "2021-10-21",
            driverName: "Jack",
            vehicleType: 1,
            atWarehouse: {
                startDate: "2021-10-20",
                endDate: "2021-10-21"
            },
            sentFromStore: {
                startDate: "2021-10-21",
                endDate: null
            },
            arrivedDestination: {
                startDate: null,
                endDate: null
            },
            isArrived: true,
            isSent: true,
            remainingTime: 0
        },
        {
            name: "Table",
            id: 132,
            price: 200,
            destination: "Birger Jarlsgatan 112,  114 20 Stockholm,  Sweden",
            source: " Norrbackagatan 15,  113 34 Stockholm,  Sweden",
            sourceLatLng: {
                lat: 59.34161634194161,
                lng: 18.030620643702203,
            },
            destinationLatLng: {
                lat: 59.34817291129667,
                lng: 18.060304824719754,
            },
            orderedDate: "2021-10-20",
            arrivalDate: "2021-10-21",
            driverName: "Jack",
            vehicleType: 1,
            atWarehouse: {
                startDate: "2021-10-20",
                endDate: "2021-10-21"
            },
            sentFromStore: {
                startDate: "2021-10-21",
                endDate: null
            },
            arrivedDestination: {
                startDate: null,
                endDate: null
            },
            isArrived: true,
            isSent: true,
            remainingTime: 0
        }
    ];
}
