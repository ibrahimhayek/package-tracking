import { Error } from './errors';


export class result<T>{
    result: T;
    errors: Error[];

    public hasError(): boolean {
        if (this.errors && this.errors.length > 0) {
            return true
        }
        return false
    }
}