import { Package } from '../../common-models/package';
import { Injectable } from '@angular/core';
import { PackagesService } from '../rest-apis/Packages.service';
import { PackagesMock } from '../mock-data/Packages-mock.service';


@Injectable({
    providedIn: 'root',
    useClass: PackagesMock,
    // useClass: PackagesService, IF USING REST API CALLS
})
export abstract class IPackagesBase {
    abstract retrievePackages(): Promise<Package[]>;
}
