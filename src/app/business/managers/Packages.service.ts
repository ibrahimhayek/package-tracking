import { IPackagesBase } from '../reprositories/IPackages';
import { Package } from '../../common-models/package';
import { result } from '../models/result';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PackagesManager {
  constructor(pack: IPackagesBase) {
    this._package = pack;
  }
  _package: IPackagesBase;

  async retrievePackages(): Promise<result<Package[]>> {
    var output = new result<Package[]>();
    output.result = await this._package.retrievePackages();
    return Promise.resolve<result<Package[]>>(output);
  }
}
