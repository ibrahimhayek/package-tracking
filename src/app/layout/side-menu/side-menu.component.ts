import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { HelperService } from 'src/app/core/services/helper.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  constructor(
    private router: Router,
    private translate: TranslateService,
    private helperService: HelperService
  ) { }

  opened: boolean = false;
  languages = [
    {
      img: 'assets/images/englishFlag.png',
      label: 'English',
      value: 'en',
    },
    {
      img: 'assets/images/swedenFlag.png',
      label: 'Swedish',
      value: 'sw',
    }
  ];
  language = '';

  ngOnInit(): void {
    this.setDefaultLanguage()
  }

  setDefaultLanguage() {
    this.language = this.helperService.getCurrentLanguage();
  }

  changeLanguage(event) {
    this.language = event.value;
    localStorage.setItem('currentLanguage', event.value);
    this.translate.use(event.value);
  }

}
