import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatStepperModule } from '@angular/material/stepper';
import { MatCardModule } from '@angular/material/card';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';

const BASE_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  MatSidenavModule,
  MatIconModule,
  MatStepperModule,
  MatCardModule,
  IvyCarouselModule,
  MatSelectModule,
  MatInputModule
];

const COMPONENTS = [
];

const PIPES = [
];

const SHARED_MODULES = [
];

const MODULES = [

];

@NgModule({
  declarations: [...COMPONENTS, ...PIPES],
  imports: [...MODULES, ...SHARED_MODULES, ...BASE_MODULES, ToastrModule.forRoot()],
  exports: [...BASE_MODULES, ...SHARED_MODULES, ...COMPONENTS, ...PIPES, ToastrModule.forRoot()]
})
export class SharedModule { }