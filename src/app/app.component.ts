import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private translateService: TranslateService,
  ) {
    this.translateService.setDefaultLang('en');
    this.translateService.use('en');
  }

  title = 'package-tracking';
  language = '';

  ngOnInit(): void {
    if (!localStorage.getItem('currentLanguage')) {
      localStorage.setItem('currentLanguage', 'en')
      this.language = 'en';
      this.translateService.use('en');
    }
    else {
      this.translateService.use(localStorage.getItem('currentLanguage'));
    }
  }

}
