import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { PackagesManager } from 'src/app/business/managers/Packages.service';
import { HelperService } from 'src/app/core/services/helper.service';
import { Package } from '../../common-models/package';
import { TravelMarker, TravelMarkerOptions, TravelData, EventType } from 'travel-marker';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { CountdownConfig } from 'ngx-countdown';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})

export class TimelineComponent implements OnInit {

  constructor(
    PackagesManager: PackagesManager,
    private helperService: HelperService
  ) {
    this._packagesManager = PackagesManager
  }

  @ViewChild("placesRef") placesRef: GooglePlaceDirective;
  myOrders = [];
  _packagesManager: PackagesManager;
  selectedOrder: Package;
  OpenedStepperIndex = 0;
  zoom: number = 8;
  origin: any;
  destination: any;
  travelMode = 'DRIVING';
  routePoints: any;
  map: any;
  line: any;
  marker: TravelMarker = null;
  speedMultiplier = 1;
  autoCompleteAddress = '';
  renderOptions = {
    suppressMarkers: true,
  };
  travelModes = [
    {
      value: 'DRIVING',
      label: 'Car',
      img: 'assets/images/truck.png'
    },
    {
      value: 'BICYCLING',
      label: 'bike',
      img: 'assets/images/bike.png'
    }
  ]
  markerOptions = {
    origin: {
      icon: 'assets/images/warehouse.png',
      label: '',
    },
    destination: {
      icon: 'assets/images/destination.png',
      label: '',
      draggable: true,
    },
  };
  vehicleLocation = {
    lat: null,
    lng: null
  };
  CountdownTimeUnits: Array<[string, number]> = [
    ['Y', 1000 * 60 * 60 * 24 * 365],
    ['M', 1000 * 60 * 60 * 24 * 30],
    ['D', 1000 * 60 * 60 * 24],
    ['H', 1000 * 60 * 60],
    ['m', 1000 * 60],
    ['s', 1000],
    ['S', 1],
  ];
  countdownConfig: CountdownConfig;

  ngOnInit(): void {
    this.getPackages();
  }

  // PAGE & DATA FUNCTIONS
  async getPackages() {
    await this._packagesManager.retrievePackages().then(response => {
      let resp = response;
      if (resp.hasError()) {
        this.showFaildToastr('Error', 'Failed to load packages.')
      }
      else {
        this.myOrders = response.result;
        if (this.myOrders.length > 0) {
          this.selectedOrder = this.myOrders[0];
          this.orderClicked(this.selectedOrder, 0);
          this.displayLocation(this.selectedOrder.destinationLatLng.lat, this.selectedOrder.destinationLatLng.lng);
        }
        console.log(this.myOrders);
      }
    });
  }
  orderClicked(order, index) {
    this.selectedOrder = order;
    this.getTimeRemaining(this.selectedOrder.remainingTime);
    if (this.selectedOrder.isSent && !this.selectedOrder.isArrived) {
      this.OpenedStepperIndex = 1
    }
    else if (this.selectedOrder.isArrived) {
      this.OpenedStepperIndex = 2
    }
    else {
      this.OpenedStepperIndex = 0
    };
    this.selectedOrder.vehicleType == 1 ? this.travelMode = 'DRIVING' : this.travelMode = 'BICYCLING';
    this.displayLocation(this.selectedOrder.destinationLatLng.lat, this.selectedOrder.destinationLatLng.lng);
    this.setMapLinks();
  }
  showFaildToastr(title, message) {
    this.helperService.showFaildToastr(title, message);
  }
  showSuccessToastr(title, message) {
    this.helperService.showSuccessToastr(title, message);
  }
  vehicleTypeChange(event) {
    event.value == 'DRIVING' ? this.selectedOrder.vehicleType = 1 : this.selectedOrder.vehicleType = 2
    this.initRoute(event.value)
  }

  // GOOGLE PLACES FUNCTIONS
  handleAddressChange(address: Address) {
    this.selectedOrder.destinationLatLng.lat = address.geometry.location.lat();
    this.selectedOrder.destinationLatLng.lng = address.geometry.location.lng();
    this.selectedOrder.destination = address.formatted_address;
    this.showSuccessToastr('Change destination', 'Destination has been changed');
    this.setMapLinks();
  }

  // MAP FUNCTIONS
  displayLocation(latitude, longitude) {
    var geocoder;
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(latitude, longitude);
    let that = this;
    geocoder.geocode(
      { 'latLng': latlng },
      function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            var add = results[0].formatted_address;
            var value = add.split(",");
            let count = value.length;
            let country = value[count - 1];
            let state = value[count - 2];
            let city = value[count - 3];
            that.autoCompleteAddress = ` ${city}, ${state}, ${country}`;
            that.selectedOrder.destination = that.autoCompleteAddress;
          }
        }
      }
    );
  }
  onMapReady(event) {
    this.map = event;
  }
  initRoute(vehicleType) {
    if (this.routePoints && this.routePoints.length > 0) {
      if (this.marker) {
        this.marker.setMap(null);
      }
      const locationArray = this.routePoints.map(l => new google.maps.LatLng(l[0], l[1]));
      this.line = new google.maps.Polyline({
        strokeOpacity: 0,
        path: [],
        map: this.map
      });
      locationArray.forEach(l => this.line.getPath().push(l));
      const route = this.line.getPath().getArray();
      const options: TravelMarkerOptions = {
        map: this.map,
        speed: 50,
        interval: 10,
        speedMultiplier: this.speedMultiplier,
        markerOptions: {
          title: 'Travel Marker',
          animation: google.maps.Animation.DROP,
          icon: vehicleType == 'DRIVING' ? 'assets/images/truck.png' : 'assets/images/bike.png',
        },
      };
      let currentRoutes = [];
      if (!this.selectedOrder.isSent) {
        currentRoutes.push(route[1]);
      }
      else if (this.selectedOrder.isSent && !this.selectedOrder.isArrived) {
        route.forEach((element, index) => {
          if (index < route.length / 2) {
            currentRoutes.push(element);
          }
        });
      }
      else {
        currentRoutes = route;
        options.speed = 300;
      };
      this.marker = new TravelMarker(options);
      this.marker.addLocation(currentRoutes);
      this.marker.event.onEvent((event: EventType, data: TravelData) => {
        this.vehicleLocation.lat = data.location.lat();
        this.vehicleLocation.lng = data.location.lng();
      });
      this.play()
    }
  }
  play() {
    this.marker.play();
  }
  onMarkerChange(event: any) {
    this.routePoints = [];
    event.routes[0].overview_path.forEach(element => {
      let point = [];
      point.push(element.lat());
      point.push(element.lng());
      this.routePoints.push(point);
    });
    let vehicle = '';
    this.selectedOrder.vehicleType == 1 ? vehicle = 'DRIVING' : vehicle = 'BICYCLING';
    this.initRoute(vehicle);
  }
  markerDragEnd(event) {
    this.selectedOrder.destinationLatLng.lat = event.lat();
    this.selectedOrder.destinationLatLng.lng = event.lng();
    this.displayLocation(this.selectedOrder.destinationLatLng.lat, this.selectedOrder.destinationLatLng.lng);
    this.setMapLinks();
  }
  mapClicked(event) {
    this.selectedOrder.destinationLatLng.lat = event.coords.lat;
    this.selectedOrder.destinationLatLng.lng = event.coords.lng;
    this.displayLocation(this.selectedOrder.destinationLatLng.lat, this.selectedOrder.destinationLatLng.lng);
    this.setMapLinks();
  }
  setMapLinks() {
    this.origin = {
      lat: this.selectedOrder.sourceLatLng.lat,
      lng: this.selectedOrder.sourceLatLng.lng,
    }
    this.destination = {
      lat: this.selectedOrder.destinationLatLng.lat,
      lng: this.selectedOrder.destinationLatLng.lng,
    };
    let vehicle = '';
    this.selectedOrder.vehicleType == 1 ? vehicle = 'DRIVING' : vehicle = 'BICYCLING';
    this.initRoute(vehicle);
  }

  // TIME REMAINING FUNCTIONS

  getTimeRemaining(remainingTime) {
    this.countdownConfig = {
      leftTime: 60 * 60 * remainingTime,
      formatDate: ({ date, formatStr }) => {
        let duration = Number(date || 0);

        return this.CountdownTimeUnits.reduce((current, [name, unit]) => {
          if (current.indexOf(name) !== -1) {
            const v = Math.floor(duration / unit);
            duration -= v * unit;
            return current.replace(new RegExp(`${name}+`, 'g'), (match: string) => {
              return v.toString().padStart(match.length, '0');
            });
          }
          return current;
        }, formatStr);
      },
    };
  }
}




